<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8" http-equiv="content-type"  content="text/html"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
   <TITLE>Romy R Michael</TITLE>
    <META NAME="description" CONTENT="Romy R Michael is a creative web UI Frond end designer and developer in Kerala, India. He has specialized in HTML5, CSS3, jQuery, SASS, Responsive Design, Mobile friendly websites, Flash As2, PHP, Mel, JavaScript with a powerfull experience working along with a Spacilized Team in a reputed company in Technopark Trivandrum">
    <META NAME="keywords" CONTENT="Romy R Michael, RomyRMichael, romyrmichael, Webdesigner Trivandrum, Webdesign Freelancer in Trivandrum, HTML5 Developer, CSS3 Developer, jQuery developer, Best Webdesigning expert in Trivandrum, UI developer in Technopark, Best Ui developer in Kerala, Joomla Developer, Joomla Websites, Codeigniter, Best websites 2015, Web application developer in Trivandrum, Best web designing employee in technopark, Best web designing person working in technopark, best web designer in India.">
    <META NAME="robot" CONTENT="index,follow">
    <META NAME="refresh" CONTENT="3000">
    <META NAME="copyright" CONTENT="Copyright &copy;  RojesWebLab <?php echo(date("Y"));?>">
    <META NAME="author" CONTENT="Romy R Michael">
    <META NAME="language" CONTENT="English">
    <META NAME="revisit-after" CONTENT="2 days">


    <!-- Bootstrap Core CSS - Uses Bootswatch Flatly Theme: http://bootswatch.com/flatly/ -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/style.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="js/vegas/jquery.vegas.css" />
    <link rel="stylesheet" type="text/css" href="css/dripicon.css" />
    <link rel="stylesheet" type="text/css" href="css/swipebox.css" />
	<link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico"/>
    <!-- Custom Fonts -->
    <link href="font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
    [data-sr] {
        visibility: hidden;
    }
    </style>

</head>

<body id="page-top" class="index">

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-bottom navbar-wrapper">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#page-top">
                    UID<strong> ROMY </strong>R Michael
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">



                    <li class="page-scroll">
                        <a href="#about"><i class="icon-user"></i>Profile</a>
                    </li>

                    <li class="page-scroll">
                        <a href="#features"><i class="icon-attachment"></i>Resume</a>
                    </li>

                    <li class="page-scroll">
                        <a href="#pricing"><i class="icon-camera"></i>Skill</a>
                    </li>

                    <li class="page-scroll">
                        <a href="#portfolio"><i class="icon-folder-open"></i>Portfolio</a>
                    </li>

                    <li class="page-scroll">
                        <a href="#contact"><i class="icon-mail"></i>Contact</a>
                    </li>

                    <!--<li class="page-scroll">
                        <a href="component.html"><i class="icon-window"></i>Component</a>
                    </li>-->


                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid-fluid -->
    </nav>
    <!-- End of Navigation -->

    <!-- Header -->
    <header>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="intro-text">
                        <h1>
                            <span>Romy R Michael</span>
                        </h1>
                        <h4>
                            <span>Web <strong>UI Developer</strong> & UI Designer, <br/>3D Visulize & FX Artist</span>
                        </h4>
                        <br>
                        <span class="page-scroll">
                            <a class="color-white" href="#about">Explore</a>
                        </span>
                    </div>                  
                </div>
            </div>
            
        </div>
         <!--<div class="embed-responsive embed-responsive-16by9">
                      <iframe class="embed-responsive-item" src="http://www.youtube.com/embed/eoK4i0SU3DA"></iframe>
                    </div> 
                 -->
          <video width="100%"  class="bgGraphic" poster="img/bgPoster.jpg" preload="auto" muted autoplay loop >
                      <source src="img/clouds.mp4" type="video/mp4">
                  </video> 

    </header>
    <!-- End of Header -->

    <!-- Profile Section -->
    <section class="e-campuz" id="about">
        <div class="ray ray1"></div>
        <div class="ray ray2"></div>
        <div class="ray ray3"></div>

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div data-sr="reset wait 1s enter top and move 150px" class="title-overview">
                        <h2>
                            <span class="border-white">About UIDR</span>
                        </h2>
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-sm-4">
                    <img alt="" data-sr="reset wait 1s enter bottom and move 150px" class="admin-pic img-circle" src="img/23.jpg">

                </div>
                <div class="col-sm-8">
                    <div class="about-nest">
                        <h2 data-sr="reset wait 1.5s enter left please, and hustle 20px">Hello, I am
                            <strong>Romy R Michael</strong>
                            <br>Web UI Designer/Developer</h2>
                        <br>

                        <p data-sr="reset enter left please, and hustle 20px">" Romy is the calm, serene and holistic ingredient at QTL. His eagerness to learn and achieve magical results has earned him the title of the wall. He is the final answer to the toughest situations. This young guy reflects a personality and experience twice his age. Even his codes look like crisp artwork." 
                       </p>
                        <h4 class="text-right">By Quintessence Technologies Pvt Ltd.</h4>
                        <ul data-sr='reset enter right please, and hustle 40px' class="about-detail">
                            <li><i class="icon-user-group"></i>Male</li>
                            <li><i class="icon-heart"></i>Born on 19<sup>Th</sup> May 1983</li>
                            <li><i class="icon-location"></i>Vattappara, Trivandrum, Kerala, India</li>
                            <li><i class="icon-gaming"></i>Traveling</li>
                            <li><i class="icon-music"></i>Rock, Pop, Jazz, HIPHop ,Reage and R&amp;B</li>

                        </ul>

                        <ul data-sr='reset wait 1.5s enter right please, and hustle 40px' class="list-inline pull-right social-list">
                            <li>
                                <a href="https://www.facebook.com/romy.r.michael" class="btn-social btn-outline"><i class="fa fa-fw fa-facebook"></i></a>
                            </li>
                            <li>
                                <a href="https://plus.google.com/u/0/112480879222026671820/posts" class="btn-social btn-outline"><i class="fa fa-fw fa-google-plus"></i></a>
                            </li>
                            <li>
                                <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-twitter"></i></a>
                            </li>

                        </ul>

                    </div>
                </div>
            </div>
        </div>

        <div class="ray-bottom ray1"></div>
        <div class="ray-bottom ray2"></div>
        <div class="ray-bottom ray3"></div>
    </section>
    <!-- End of Profile Section -->



    <!-- Resume Section -->
    <section class="success" id="features">
        <div class="arrow-down"></div>
        <div class="ray-top ray1"></div>
        <div class="ray-top ray2"></div>
        <div class="ray-top ray3"></div>

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="title-overview">
                        <h2 class="color-yellow title-change-color">
                            <span class="border-color">Resume</span>
                        </h2>
                    </div>
                </div>
            </div>

            
            <div class="row">
                <div class="col-md-4">
                    <div class="resume-content">
                        <h3>Experience</h3>
                    </div>
                </div>


                <div class="col-md-8">
                    <div class="resume-content">
                        <h4>Web UI Developer / 3d Vizualizer</h4>
                        <h5>
                            <span><i class="icon-calendar"></i>Nov 4<sup>th</sup> 2015 - <?php echo(date("d-M-Y")); ?></span>
                        </h5>
                        <p>BuildNext.in, Kerala, India</p>
                        <p>BuildNext.in - <a hrer="http://www.buildnext.in">http://www.buildnext.in</a>, <a hrer="http://studio.buildnext.in">http://studio.buildnext.in</a></p>
                        <hr>
                        <h4>Web VR 360</h4>
                        <h5>
                            <span><i class="icon-calendar"></i>Jan 4<sup>th</sup> 2017 - <?php echo(date("d-M-Y")); ?></span>
                        </h5>
                        <p>Trivandrum, Kerala, India</p>
                        <p>MyProjects - <a target="_blank" hrer="http://romyrmichael.co.nf/webvr/">http://romyrmichael.co.nf/webvr/</a></p>
                        <hr>
                        <h4>Freelance Web UI Developer / Designer</h4>
                        <h5>
                            <span><i class="icon-calendar"></i>Sep 1<sup>St</sup> 2015 - </i>Nov 1<sup>St</sup> 2015</span>
                        </h5>
                        <p>In a well reputed group in Trivandrumm , Kerala, India</p>
                        <p>RojesMedia</p>
                        <hr>
                        <h4>UI Front End Developer / Designer</h4>
                        <h5>
                            <span><i class="icon-calendar"></i>Feb 1<sup>St</sup> 2011 - Aug 31<sup>St</sup> 2015</span>
                        </h5>
                        <p>In a well reputed company in Technopark Trivandrumm , Kerala, India</p>
                        <p>Quintessence Technologies Ltd</p>
                        <p>Web : <a href="http://www.qtlindia.com/" target="_blank">http://www.qtlindia.com/</a></p>
                        <hr>

                        <h4>Application Engineer </h4>
                        <h5>
                            <span><i class="icon-calendar"></i>2006 - 2008</span>
                        </h5>
                        <p>Product Support for Adobe, Autodesk, Microsoft at Scott Technologies Ltd. Trivandrum</p>
                        <hr>

                        
                    </div>


                </div>
            </div>
            <br>
            <br>

            <div class="row">
                <div class="col-md-4">
                    <div class="resume-content">
                        <h3>Education</h3>

                    </div>

                </div>


                <div class="col-md-8">
                    <div class="resume-content">
                        <h4>Kerala University</h4>
                        <h5>
                            <span><i class="icon-calendar"></i>2012 - 2015</span>
                        </h5>
                        <p>BSc Computer Science</p>
                        <hr>

                        <h4>Keltron Animaton</h4>
                        <h5>
                            <span><i class="icon-calendar"></i>2004 - 2006</span>
                        </h5>
                        <p>Diploma in Multimedia and Animation [Specialized in Web Technology]</p>
                        <hr>

                        <h4>Computer Hard Ware and Networking</h4>
                        <h5>
                            <span><i class="icon-calendar"></i>2002 - 2010</span>
                        </h5>
                        <p>Advanced Diploma in Computer Hardware maintenance and networking </p>
                        <hr>
                    </div>


                </div>
            </div>

            <br>
            <br>
        </div>
    </section>
    <!-- End of Resume Section -->


    <!-- Skill Section -->
    <section class="e-campuz" id="pricing">
        <div class="arrow-down-white"></div>
        <div class="ray-top ray1"></div>
        <div class="ray-top ray2"></div>
        <div class="ray-top ray3"></div>

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="title-overview">
                        <h2>
                            <span class="border-white">Skill</span>
                        </h2>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="skill-desc color-white">
                        <h3>Let's do some
                            <strong>Magic!</strong>
                        </h3>
                        <p>Man behind the gun, any sophisticated weapons. Human remains that taking the role. I'm experienced in utilizing all resources, for best results and perfect quality</p>
                    </div>

                    <!-- Code Start -->
                    <div class="chart-horiz clearfix">
                        <!-- Actual bar chart -->
                        <ul class="chart">
                            <li class="title"></li>
                            <li class="current" title="HTML 5">
                                <span class="bar" data-number="98"></span>
                                <span class="number">98%</span>
                            </li>
                            <li class="current" title="SVG 4">
                                <span class="bar" data-number="84"></span>
                                <span class="number">84%</span>
                            </li>
                            <li class="current" title="three.js">
                                <span class="bar" data-number="55"></span>
                                <span class="number">55%</span>
                            </li>
                            <li class="current" title="babylon.js">
                                <span class="bar" data-number="87"></span>
                                <span class="number">87%</span>
                            </li>
                            <li class="current" title="Web VR">
                                <span class="bar" data-number="90"></span>
                                <span class="number">90%</span>
                            </li>
                            <li class="current" title="CSS 3">
                                <span class="bar" data-number="97"></span>
                                <span class="number">97%</span>
                            </li>
                            <li class="current" title="jQuery">
                                <span class="bar" data-number="94"></span>
                                <span class="number">94%</span>
                            </li>
                            <li class="current" title="Responsive">
                                <span class="bar" data-number="98"></span>
                                <span class="number">98%</span>
                            </li>
                            <li class="current" title="SASS">
                                <span class="bar" data-number="86"></span>
                                <span class="number">86%</span>
                            </li>
                            <li class="current" title="PHP">
                                <span class="bar" data-number="15"></span>
                                <span class="number">15%</span>
                            </li>
                            <li class="current" title="Photoshop">
                                <span class="bar" data-number="98"></span>
                                <span class="number">98%</span>
                            </li>
                            <li class="current" title="Adobe Brackets">
                                <span class="bar" data-number="92"></span>
                                <span class="number">92%</span>
                            </li>
                            <li class="current" title="Adobe Ilustrator">
                                <span class="bar" data-number="96"></span>
                                <span class="number">96%</span>
                            </li>
                            <li class="current" title="Adobe AfterFX">
                                <span class="bar" data-number="50"></span>
                                <span class="number">50%</span>
                            </li>
                            <li class="current" title="Adobe Audition">
                                <span class="bar" data-number="80"></span>
                                <span class="number">80%</span>
                            </li>
                            <li class="current" title="Cubase 7 Elements SE">
                                <span class="bar" data-number="50"></span>
                                <span class="number">50%</span>
                            </li>
                            <li class="current" title="Corel Draw">
                                <span class="bar" data-number="90"></span>
                                <span class="number">90%</span>
                            </li>
                            <li class="current" title="Maya 2016">
                                <span class="bar" data-number="70"></span>
                                <span class="number">70%</span>
                            </li>
                            <li class="current" title="Blender">
                                <span class="bar" data-number="80"></span>
                                <span class="number">80%</span>
                            </li>
                            <li class="current" title="Unity 3D">
                                <span class="bar" data-number="40"></span>
                                <span class="number">40%</span>
                            </li>
                            <li class="current" title="Web GL 3D">
                                <span class="bar" data-number="60"></span>
                                <span class="number">60%</span>
                            </li>
                        </ul>
                    </div>



                </div>
            </div>
        </div>
        <div class="ray-bottom ray1"></div>
        <div class="ray-bottom ray2"></div>
        <div class="ray-bottom ray3"></div>
    </section>
    <!-- End of Skill Section -->


    <!-- Portfolio Section -->
    <section id="portfolio">
        <div class="ray-top ray1"></div>
        <div class="ray-top ray2"></div>
        <div class="ray-top ray3"></div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="title-overview">
                        <h2 class="color-yellow title-change-color">
                            <span class="border-color">Porftolio</span>
                        </h2>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">

                    <ul id="filters" class="clearfix">
                    	
                        <li>
                            <span class="filter active" data-filter="app card icon logo web">All</span>
                        </li>                                               
                        <li>
                            <span class="filter" data-filter="web">Web</span>
                        </li>
                        <li>
                            <span class="filter" data-filter="Music">Music</span>
                        </li>                        
                        <li>
                            <span class="filter" data-filter="VFX">VFX</span>
                        </li> 
                    </ul>

                    <div id="portfoliolist">
                        <div class="portfolio web" data-cat="web">
                            <div class="portfolio-wrapper">
                                <a title="Click the link below to view 360 VR" class="swipebox" href="img/portfolios/web/3.jpg">
                                    <img class="imagelightbox" src="img/portfolios/web/3.jpg" alt="" />
                                </a>
                                <div class="label">
                                    <div class="label-text">
                                        <a class="text-title" href="webvr/" target="_blank">Web VR 360 Panorama</a>
                                        <span class="text-category">360 deg Web VR</span>
                                    </div>
                                    <div class="label-bg"></div>
                                </div>
                            </div>
                        </div>
                        <div class="portfolio web" data-cat="web">
                            <div class="portfolio-wrapper">
                                <a title="Unite" class="swipebox" href="img/portfolios/web/1.jpg">
                                    <img class="imagelightbox" src="img/portfolios/web/1.jpg" alt="" />
                                </a>
                                <div class="label">
                                    <div class="label-text">
                                        <a class="text-title" href="http://u4hc.com/index.php" target="_blank">Unite</a>
                                        <span class="text-category">Responsive Web</span>
                                    </div>
                                    <div class="label-bg"></div>
                                </div>
                            </div>
                        </div>
                        <div class="portfolio web" data-cat="web">
                            <div class="portfolio-wrapper">
                                <a title="MantriSquare" class="swipebox" href="img/portfolios/web/2.jpg">
                                    <img class="imagelightbox" src="img/portfolios/web/2.jpg" alt="" />
                                </a>
                                <div class="label">
                                    <div class="label-text">
                                        <a class="text-title" href="http://mantrisquare.com/" target="_blank">mantrisquare.com</a>
                                        <span class="text-category">Responsive Web</span>
                                    </div>
                                    <div class="label-bg"></div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="portfolio web" data-cat="web">
                            <div class="portfolio-wrapper">
                                <a title="Srijit Associates" class="swipebox" href="img/portfolios/web/4.jpg">
                                    <img class="imagelightbox" src="img/portfolios/web/4.jpg" alt="" />
                                </a>
                                <div class="label">
                                <div class="label">
                                    <div class="label-text">
                                        <a class="text-title" href="http://www.srijitassociates.com/" target="_blank">
                                            www.srijitassociates.com</a>
                                        <span class="text-category">Responsive Web</span>
                                    </div>
                                    <div class="label-bg"></div>
                                </div>
                            </div>
                        </div>
                       
                    </div>
                </div>
                <div class="col-lg-12">  
                       <script type="text/javascript">
							google_ad_client = "ca-pub-5413044079732593";
							google_ad_slot = "6906959262";
							google_ad_width = 728;
							google_ad_height = 90;
						</script>
						<!-- http://jqplugins.co.nf/fxSlider/ -->
						<script type="text/javascript"
						src="//pagead2.googlesyndication.com/pagead/show_ads.js">
						</script>
                </div>
            </div>
        </div>
    <!-- <div class="ray-bottom ray1"></div>
        <div class="ray-bottom ray2"></div>
        <div class="ray-bottom ray3"></div> -->
    </section>
    <!-- End of Portfolio Section -->



    <!-- Footer -->
    <footer id="contact" class="text-center footer-above">
        <div class="arrow-down-white"></div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div style="position:relative;z-index:999" class="title-overview">
                        <h2>
                            <span class="border-grey color-grey">Contact</span>
                        </h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div id="test1" class="gmap3"></div>



                <div class="col-md-8 col-md-offset-2">
                    <address class="text-center nest-address">


                        <i class="icon-location"></i>&nbsp; Kerala, India

                        <i class="icon-home"></i>&nbsp;Trivandrum

                        <abbr title="Phone">
                            <i class="icon-phone"></i>
                        </abbr>&nbsp;+91 9496023417</address>

                    <form role="form" style="position:relative;z-index:9999;">
                        <div class="row">
                            <div class="col-md-6">
                                <input type="text" class="form-control form-footer" placeholder="Your Name">
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control form-footer" placeholder="Subject">
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12">

                                <textarea class="form-control form-footer" rows="5" placeholder="Message"></textarea>
                            </div>

                        </div>

                        <br>
                        <button type="submit" class="btn bg-black text-center">Submit</button>

                    </form>
                    <br>
                </div>
            </div>
        </div>
    </footer>

    <div class="footer-below">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <!--Made with <i class="icon-heart"></i> in Yogyakarta,-->
                    Copyright &copy; <a href="#"> RojesWebLab</a> <?php echo(date("Y")); ?>
                </div>
            </div>
        </div>
    </div>
    <!-- End of Footer -->


    <!-- Setting -->
    <div class="slide-out-div">
        <a class="handle" href="http://link-for-non-js-users/"><i class="icon-gear"></i></a>

        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 pad-reset">
                    <div class="no-border handle-tittle">Color Scheme</div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 pad-reset">
                    <button type="button" id="click-orange" class="btn btn-block no-border bg-orange "></button>
                </div>
                <div class="col-sm-6 pad-reset">
                    <button type="button" id="click-blue" class="btn btn-block no-border bg-blue"></button>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4 pad-reset">
                    <button type="button" id="click-red" class="btn btn-block no-border bg-red"></button>
                </div>
                <div class="col-sm-4 pad-reset">
                    <button type="button" id="click-green" class="btn btn-block no-border bg-green"></button>
                </div>
                <div class="col-sm-4 pad-reset">
                    <button type="button" id="click-purple" class="btn btn-block no-border bg-purple"></button>
                </div>
            </div>
        </div>


        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 pad-reset">
                    <div class="no-border handle-tittle">Background</div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 pad-reset">
                    <button type="button" id="click-bg-dark" class="btn btn-block no-border bg-black"></button>
                </div>
                <div class="row">
                </div>
                <div class="col-sm-12 pad-reset">
                    <button type="button" id="click-bg-light" class="btn btn-block no-border bg-grey"></button>
                </div>

            </div>
        </div>


    </div>
    <!-- End of Setting -->



    <!-- jQuery Version 1.11.0 -->
    <!--<script src="js/jquery-1.11.0.js"></script>-->
	<script type="text/javascript" src="js/jquery-2.1.3.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/style-change.js"></script>

    <!-- Plugin JavaScript -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="js/classie.js"></script>
    <script src="js/cbpAnimatedHeader.js"></script>

    <!-- Contact Form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="js/freelancer.js"></script>
    <script src="js/horizBarChart.js"></script>
    <!-- portfolio filtered -->
    <script type="text/javascript" src="js/jquery.easing.min.js"></script>
    <script type="text/javascript" src="js/jquery.mixitup.min.js"></script>
    <script type="text/javascript" src="js/jquery.swipebox.js"></script>
    <script type="text/javascript" src="js/app.js"></script>
    <!-- Gmap  Option -->

    <script src="http://maps.googleapis.com/maps/api/js?sensor=false" type="text/javascript"></script>
    <script type="text/javascript" src="js/gmap3.min.js"></script>
    <!--  Sliding settting -->
    <script src="js/jquery.tabSlideOut.v1.3.js"></script>
    <script src='js/scrollReveal.js'></script>
    <script>
    window.sr = new scrollReveal();
    </script>


    <script type="text/javascript">
    $(function() {
        $('#test1').gmap3({
            marker: {
                address: "Vattappara, Kerala"
            },
            map: {
                options: {
                    zoom: 16
                }
            }
        });
    });
    </script>


    <script type="text/javascript">
    $(function() {
        //lightbox 
        $('.swipebox').swipebox();
        //filtered portfolio
        var filterList = {
            init: function() {
                // MixItUp plugin
                // http://mixitup.io
                $('#portfoliolist').mixitup({
                    targetSelector: '.portfolio',
                    filterSelector: '.filter',
                    effects: ['fade'],
                    easing: 'snap',
                    // call the hover effect
                    onMixEnd: filterList.hoverEffect()
                });

            },

            hoverEffect: function() {

                // Simple parallax effect
                $('#portfoliolist .portfolio').hover(
                    function() {
                        $(this).find('.label').stop().animate({
                            bottom: 0
                        }, 200, 'easeOutQuad');
                        $(this).find('img').stop().animate({
                            top: -30
                        }, 500, 'easeOutQuad');
                    },
                    function() {
                        $(this).find('.label').stop().animate({
                            bottom: -40
                        }, 200, 'easeInQuad');
                        $(this).find('img').stop().animate({
                            top: 0
                        }, 300, 'easeOutQuad');
                    }
                );

            }

        };

        // Run the show!
        filterList.init();
        //slide out panel
        $('.slide-out-div').tabSlideOut({
            tabHandle: '.handle', //class of the element that will be your tab
            pathToTabImage: '<i class="icon-phone"></i>', //path to the image for the tab (optionaly can be set using css)
            imageHeight: '40px', //height of tab image
            imageWidth: '40px', //width of tab image    
            tabLocation: 'right', //side of screen where tab lives, top, right, bottom, or left
            speed: 300, //speed of animation
            action: 'click', //options: 'click' or 'hover', action to trigger animation
            topPos: '200px', //position from the top
            fixedPosition: true //options: true makes it stick(fixed position) on scroll
        });

    });
    </script>

</body>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-41309191-2', 'auto');
  ga('send', 'pageview');

</script>
</html>
